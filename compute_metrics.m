%% batch script for computation of 10 different metrics for BOLD signal
% Author: Rosaleena Mohanty
% Date: 23-11-2017

% Interpretation assumes:
% N = number of ROIs; 
% S = number of subjects; 
% T = number of time-points in BOLD signal

% Folder structure:
% SOURCE_FOLDER
%   SUB01
%       VOI_ROI1.mat
%       VOI_ROI2.mat
%       VOI_ROI3.mat
%       .
%       .
%       .
%   SUB50
%       VOI_ROI1.mat
%       VOI_ROI2.mat
%       VOI_ROI3.mat

clc, close all, clear all
cd('path to folder containing individual subject folders')
d = dir('SUB*'); % list of all subjects as stored in the directory (e.g. 'SUB01','SUB13')

%% 1. correlation similarity (Pearson's correlation)
correlation_sub = [];
for i = 1:length(d)
   ts = [];
   cd(d(i).name)
   roi = dir('VOI*mat'); % reads the names of ROIs being analyzed (each VOI*.mat file containes the time course from a seed region)
   
   % this loop extracts the time courses from each ROI
   for z = 1:length(roi)
      roi_ts = load(roi(z).name); roi_ts = zscore(roi_ts.Y);
      ts(end+1,:) = roi_ts';
   end
   % ts should be N by T
   ts(~any(ts,2), : ) = []; % remove 0 rows (in case any volumes were censored)
   ts_corr = corr(ts'); % gives N by N RSFC matrix
    
   % compute the metric
   correlation_sub(end+1,:) = ts_corr(:); % store RSFC for each subject
   cd('..')
end
% correlation_sub should have S by N^2 (can extract just the lower triangle to have correlation_sub of size S by N(N-1)/2) 

%% 2. cosine similarity
cosine_sub = [];
for i = 1:length(d)
   ts = [];
   cd(d(i).name)
   roi = dir('VOI*mat'); % reads the names of ROIs being analyzed (each VOI*.mat file containes the time course from a seed region)
   
   % this loop extracts the time courses from each ROI
   for z = 1:length(roi)
      roi_ts = load(roi(z).name); roi_ts = zscore(roi_ts.Y);
      ts(end+1,:) = roi_ts';
   end
   % ts should be N by T 
   ts(~any(ts,2), : ) = []; % remove 0 rows (in case any volumes were censored)
   
   % compute the metric
   ts_cosine = pdist(ts,'cosine'); % gives 1 by N*(N-1)/2 vector (equivalent to lower triangle of N by N RSFC matrix)
   cosine_sub(end+1,:) = 1-ts_cosine; % store RSFC for each subject
   cd('..')
end
% cosine_sub should contain S by N(N-1)/2 

%% 3. euclidean dissimilarity
euclidean_sub = [];
for i = 1:length(d)
   ts = [];
   cd(d(i).name)
   roi = dir('VOI*mat'); % reads the names of ROIs being analyzed (each VOI*.mat file containes the time course from a seed region)
   
   % this loop extracts the time courses from each ROI
   for z = 1:length(roi)
      roi_ts = load(roi(z).name); roi_ts = zscore(roi_ts.Y);
      ts(end+1,:) = roi_ts';
   end
   % ts should be N by T
   ts(~any(ts,2), : ) = []; % remove 0 rows (in case any volumes were censored)
   
   % compute the metric
   ts_euclidean = pdist(ts,'euclidean'); % should be 1 by N*(N-1)/2
   euclidean_sub(end+1,:) = ts_euclidean; % store RSFC for each subject
   cd('..')
end
% euclidean_sub should be S by N(N-1)/2

%% 4. cityblock dissimilarity
cityblock_sub = [];
for i = 1:length(d)
   ts = [];
   cd(d(i).name)
   roi = dir('VOI*mat'); % reads the names of ROIs being analyzed (each VOI*.mat file containes the time course from a seed region)
   
   % this loop extracts the time courses from each ROI
   for z = 1:length(roi)
      roi_ts = load(roi(z).name); roi_ts = zscore(roi_ts.Y);
      ts(end+1,:) = roi_ts';
   end
   % ts should be N by T
   ts(~any(ts,2), : ) = []; % remove 0 rows (in case any volumes were censored)
   
   % compute the metric
   ts_cityblock = pdist(ts,'cityblock');
   cityblock_sub(end+1,:) = ts_cityblock; % store RSFC for each subject
   cd('..')
end
% cityblock_sub should be S by N(N-1)/2 

%% 5. dynamic time warping dissimilarity
dtw_dist_sub = [];
for i = 1:length(d)
   ts = [];
   cd(d(i).name)
   roi = dir('VOI*mat'); % reads the names of ROIs being analyzed (each VOI*.mat file containes the time course from a seed region)
   
   % this loop extracts the time courses from each ROI
   for z = 1:length(roi)
      roi_ts = load(roi(z).name); roi_ts = zscore(roi_ts.Y);
      ts(end+1,:) = roi_ts';
   end
   % ts should be N by T
   ts(~any(ts,2), : ) = []; % remove 0 rows (in case any volumes were censored)
   
   % compute the metric
   dtw_dist_vec = []; 
   for u = 1:length(roi)-1
       for v = u+1:length(roi)
            dtw_dist_vec(end+1) = dtw(ts(u,:),ts(v,:));
       end
   end
   % dtw_dist_vec should be 1 by N(N-1)/2
   dtw_dist_sub(end+1,:) = dtw_dist_vec; % store RSFC for each subject
   cd('..')
end
%dtw_dist_sub should be S by N(N-1)/2

%% 6. coherence similarity
coh_sub = [];
for i = 1:length(d)
   ts = [];
   cd(d(i).name)
   roi = dir('VOI*mat');% reads the names of ROIs being analyzed (each VOI*.mat file containes the time course from a seed region)
   
   % this loop extracts the time courses from each ROI
   for z = 1:length(roi)
      roi_ts = load(roi(z).name); roi_ts = zscore(roi_ts.Y);
      ts(end+1,:) = roi_ts';
   end
   % ts should be N by T
   ts(~any(ts,2), : ) = []; % remove 0 rows (in case any volumes were censored)
   ts = ts'; % transpose ts
   
   % compute the metric
   coherence = [];
   for j = 1:size(ts,2)-1
        for k = j+1:size(ts,2)
            coherence(end+1) = max(mscohere(ts(:,j),ts(:,k))); % pick the maximum
        end
   end
   % coherence should be 1 by N(N-1)/2
   coh_sub(end+1,:) = coherence; % store RSFC for each subject
   cd('..')
end
% coh_sub should be S by N(N-1)/2

%% 7. mutual information similarity (needs an external function called 'mi' - attached to email)
addpath(genpath('path-to-folder_containing-the-folder-mi'));
makeosmex
MI = [];
for i = 1:length(d)
   ts = [];
   cd(d(i).name)
   roi = dir('VOI*mat');% reads the names of ROIs being analyzed (each VOI*.mat file containes the time course from a seed region)
   
   % this loop extracts the time courses from each ROI
   for z = 1:length(roi)
      roi_ts = load(roi(z).name); roi_ts = zscore(roi_ts.Y);
      ts(end+1,:) = roi_ts';
   end
   % ts should be N by T
   ts(~any(ts,2), : ) = []; % remove 0 rows (in case any volumes were censored)
   ts=ts'; % transpose ts
   
   % compute the metric
   MI_sub = [];
   for j = 1:size(ts,2)-1
       for k = j+1:size(ts,2)
           MI_sub(end+1) = mutualinfo(ts(:,j),ts(:,k));
       end
   end
   % MI_sub should be 1 by N(N-1)/2
   MI(i,:) = MI_sub;  % store RSFC for each subject
   cd('..')
end
% MI should be S by N(N-1)/2
    
%% 8. cross-correlation similarity
crcorr_sub = [];

for i = 1:length(d)
   ts = [];
   cd(d(i).name)
   roi = dir('VOI*mat');% reads the names of ROIs being analyzed (each VOI*.mat file containes the time course from a seed region)
   
   % this loop extracts the time courses from each ROI
   for z = 1:length(roi)
      roi_ts = load(roi(z).name); roi_ts = zscore(roi_ts.Y);
      ts(end+1,:) = roi_ts';
   end
   % ts should be N by T
   ts(~any(ts,2), : ) = []; % remove 0 rows (in case any volumes were censored)
   ts=ts'; % transpose ts
   
   % compute the metric
   ts_xcorr = xcorr(ts,'coef');
   [~,id] = max(abs(ts_xcorr));
   for j = 1:length(id)
      max_ts(j) = ts_xcorr(id(j),j); % pick the maximum
   end
   xcorr_mat = reshape(max_ts,[23,23]);
   crcorr_sub(end+1,:) = xcorr_mat(find(tril(ones(size(xcorr_mat)),-1)))'; % store RSFC for each subject 
   cd('..')
end
% crcorr_sub should be S by N(N-1)/2

%% 9. wavelet coherence similarity
wcoh_sub = [];
for i = 1:length(d)
   ts = [];
   cd(d(i).name)
   roi = dir('VOI*mat');% reads the names of ROIs being analyzed (each VOI*.mat file containes the time course from a seed region)
   
   % this loop extracts the time courses from each ROI
   for z = 1:length(roi)
      roi_ts = load(roi(z).name); roi_ts = zscore(roi_ts.Y);
      ts(end+1,:) = roi_ts';
   end
   % ts should be N by T
   ts(~any(ts,2), : ) = []; % remove 0 rows (in case any volumes were censored)
   ts = ts'; % transpose ts
   
   % compute the metric
   wvcoherence = [];
   for j = 1:size(ts,2)-1
        for k = j+1:size(ts,2)
            temp=wcoherence(ts(:,j),ts(:,k));
            wvcoherence(end+1) = max(temp(:)); % pick the maximum
        end
   end
   % wvcoherence should be 1 by N(N-1)/2
   wcoh_sub(end+1,:) = wvcoherence; % store RSFC for each subject
   cd('..')
end
% wcoh_sub should be S by N(N-1)/2 

%% 10. Earth mover's distance
addpath(genpath('path-to-folder-containing-emd-2005-02'));
emd_sub = [];
for i = 1:length(d)
   ts = [];
   cd(d(i).name)
   roi = dir('VOI*mat');% reads the names of ROIs being analyzed (each VOI*.mat file containes the time course from a seed region)
   
   % this loop extracts the time courses from each ROI
   for z = 1:length(roi)
      roi_ts = load(roi(z).name); roi_ts = zscore(roi_ts.Y);
      ts(end+1,:) = roi_ts';
   end
   % ts should be N by T
   ts(~any(ts,2), : ) = []; % remove 0 rows (in case any volumes were censored)
   ts = ts'; % transpose ts
   
   % compute the metric
   emd_dist_vec = []; nbins = 10; 
   for j = 1:size(ts,2)-1
        for k = j+1:size(ts,2)
            clear ca cb ha hb
            [ca ha] = imhist(ts(:,j), nbins);
            [cb hb] = imhist(ts(:,k), nbins);
            f1 = ha; f2 = hb; % features
            w1 = ca / sum(ca); w2 = cb / sum(cb); % weights
            [flow, fval] = emd(f1, f2, w1, w2, @gdf); % emd
            emd_dist_vec(end+1) = fval; clear fval;
        end
   end
   % emd_dist_vec should be 1 by N(N-1)/2
   emd_sub(end+1,:) = emd_dist_vec; % store RSFC for each subject
   cd('..')
end
% emd_sub should be S by N(N-1)/2 

    